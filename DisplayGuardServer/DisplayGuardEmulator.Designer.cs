﻿namespace DisplayGuardServer
{
    partial class DisplayGuardEmulator
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.poButton = new DevExpress.XtraEditors.SimpleButton();
            this.hoButton = new DevExpress.XtraEditors.SimpleButton();
            this.pcButton = new DevExpress.XtraEditors.SimpleButton();
            this.psButton = new DevExpress.XtraEditors.SimpleButton();
            this.poCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.hoCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.pcCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.psCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.hsButton = new DevExpress.XtraEditors.SimpleButton();
            this.hsCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.boButton = new DevExpress.XtraEditors.SimpleButton();
            this.richTextBox = new System.Windows.Forms.RichTextBox();
            this.serialPort4 = new System.IO.Ports.SerialPort(this.components);
            this.ssButton = new DevExpress.XtraEditors.SimpleButton();
            this.ssCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.clearButton = new DevExpress.XtraEditors.SimpleButton();
            this.startButton = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.poCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hoCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.psCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hsCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssCheckEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // poButton
            // 
            this.poButton.Location = new System.Drawing.Point(27, 72);
            this.poButton.Name = "poButton";
            this.poButton.Size = new System.Drawing.Size(150, 30);
            this.poButton.TabIndex = 0;
            this.poButton.Text = "PO(Power Open)";
            this.poButton.Click += new System.EventHandler(this.OnPowerOpen);
            // 
            // hoButton
            // 
            this.hoButton.Location = new System.Drawing.Point(27, 137);
            this.hoButton.Name = "hoButton";
            this.hoButton.Size = new System.Drawing.Size(150, 30);
            this.hoButton.TabIndex = 1;
            this.hoButton.Text = "HO(HDMI Open)";
            this.hoButton.Click += new System.EventHandler(this.OnHDMIOpen);
            // 
            // pcButton
            // 
            this.pcButton.Location = new System.Drawing.Point(27, 199);
            this.pcButton.Name = "pcButton";
            this.pcButton.Size = new System.Drawing.Size(150, 30);
            this.pcButton.TabIndex = 2;
            this.pcButton.Text = "PC(Power Close)";
            this.pcButton.Click += new System.EventHandler(this.OnPowerClose);
            // 
            // psButton
            // 
            this.psButton.Location = new System.Drawing.Point(27, 256);
            this.psButton.Name = "psButton";
            this.psButton.Size = new System.Drawing.Size(150, 30);
            this.psButton.TabIndex = 3;
            this.psButton.Text = "PS(Power State)";
            this.psButton.Click += new System.EventHandler(this.OnRequestPowerState);
            // 
            // poCheckEdit
            // 
            this.poCheckEdit.Location = new System.Drawing.Point(183, 80);
            this.poCheckEdit.Name = "poCheckEdit";
            this.poCheckEdit.Properties.Caption = "success";
            this.poCheckEdit.Size = new System.Drawing.Size(75, 22);
            this.poCheckEdit.TabIndex = 4;
            // 
            // hoCheckEdit
            // 
            this.hoCheckEdit.Location = new System.Drawing.Point(183, 145);
            this.hoCheckEdit.Name = "hoCheckEdit";
            this.hoCheckEdit.Properties.Caption = "success";
            this.hoCheckEdit.Size = new System.Drawing.Size(75, 22);
            this.hoCheckEdit.TabIndex = 5;
            // 
            // pcCheckEdit
            // 
            this.pcCheckEdit.Location = new System.Drawing.Point(183, 207);
            this.pcCheckEdit.Name = "pcCheckEdit";
            this.pcCheckEdit.Properties.Caption = "success";
            this.pcCheckEdit.Size = new System.Drawing.Size(75, 22);
            this.pcCheckEdit.TabIndex = 6;
            // 
            // psCheckEdit
            // 
            this.psCheckEdit.Location = new System.Drawing.Point(183, 264);
            this.psCheckEdit.Name = "psCheckEdit";
            this.psCheckEdit.Properties.Caption = "on";
            this.psCheckEdit.Size = new System.Drawing.Size(75, 22);
            this.psCheckEdit.TabIndex = 7;
            // 
            // hsButton
            // 
            this.hsButton.Location = new System.Drawing.Point(27, 310);
            this.hsButton.Name = "hsButton";
            this.hsButton.Size = new System.Drawing.Size(150, 30);
            this.hsButton.TabIndex = 8;
            this.hsButton.Text = "HS(HDMI State)";
            this.hsButton.Click += new System.EventHandler(this.OnRequestHDMIState);
            // 
            // hsCheckEdit
            // 
            this.hsCheckEdit.Location = new System.Drawing.Point(183, 318);
            this.hsCheckEdit.Name = "hsCheckEdit";
            this.hsCheckEdit.Properties.Caption = "on";
            this.hsCheckEdit.Size = new System.Drawing.Size(75, 22);
            this.hsCheckEdit.TabIndex = 9;
            // 
            // boButton
            // 
            this.boButton.Location = new System.Drawing.Point(27, 370);
            this.boButton.Name = "boButton";
            this.boButton.Size = new System.Drawing.Size(150, 30);
            this.boButton.TabIndex = 10;
            this.boButton.Text = "BO(Box Open)";
            this.boButton.Click += new System.EventHandler(this.OnBoxOpen);
            // 
            // richTextBox
            // 
            this.richTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox.Location = new System.Drawing.Point(286, 28);
            this.richTextBox.Name = "richTextBox";
            this.richTextBox.ReadOnly = true;
            this.richTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.richTextBox.Size = new System.Drawing.Size(585, 458);
            this.richTextBox.TabIndex = 11;
            this.richTextBox.Text = "";
            // 
            // serialPort4
            // 
            this.serialPort4.PortName = "COM4";
            this.serialPort4.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.SerialPort4DataReceived);
            // 
            // ssButton
            // 
            this.ssButton.Location = new System.Drawing.Point(27, 420);
            this.ssButton.Name = "ssButton";
            this.ssButton.Size = new System.Drawing.Size(150, 30);
            this.ssButton.TabIndex = 12;
            this.ssButton.Text = "SS(Stop Signal)";
            this.ssButton.Click += new System.EventHandler(this.OnStopSignal);
            // 
            // ssCheckEdit
            // 
            this.ssCheckEdit.Location = new System.Drawing.Point(183, 418);
            this.ssCheckEdit.Name = "ssCheckEdit";
            this.ssCheckEdit.Properties.Caption = "success";
            this.ssCheckEdit.Size = new System.Drawing.Size(75, 22);
            this.ssCheckEdit.TabIndex = 13;
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(27, 471);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(75, 37);
            this.clearButton.TabIndex = 14;
            this.clearButton.Text = "Clear Log";
            this.clearButton.Click += new System.EventHandler(this.OnClear);
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(4, 13);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(98, 33);
            this.startButton.TabIndex = 15;
            this.startButton.Text = "Start";
            this.startButton.Click += new System.EventHandler(this.OnStart);
            // 
            // labelControl
            // 
            this.labelControl.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl.Location = new System.Drawing.Point(108, 13);
            this.labelControl.Name = "labelControl";
            this.labelControl.Size = new System.Drawing.Size(172, 33);
            this.labelControl.TabIndex = 16;
            this.labelControl.Text = "Unknown";
            // 
            // DisplayGuardEmulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelControl);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.ssCheckEdit);
            this.Controls.Add(this.ssButton);
            this.Controls.Add(this.richTextBox);
            this.Controls.Add(this.boButton);
            this.Controls.Add(this.hsCheckEdit);
            this.Controls.Add(this.hsButton);
            this.Controls.Add(this.psCheckEdit);
            this.Controls.Add(this.pcCheckEdit);
            this.Controls.Add(this.hoCheckEdit);
            this.Controls.Add(this.poCheckEdit);
            this.Controls.Add(this.psButton);
            this.Controls.Add(this.pcButton);
            this.Controls.Add(this.hoButton);
            this.Controls.Add(this.poButton);
            this.Name = "DisplayGuardEmulator";
            this.Size = new System.Drawing.Size(900, 564);
            ((System.ComponentModel.ISupportInitialize)(this.poCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hoCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.psCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hsCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssCheckEdit.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton poButton;
        private DevExpress.XtraEditors.SimpleButton hoButton;
        private DevExpress.XtraEditors.SimpleButton pcButton;
        private DevExpress.XtraEditors.SimpleButton psButton;
        private DevExpress.XtraEditors.CheckEdit poCheckEdit;
        private DevExpress.XtraEditors.CheckEdit hoCheckEdit;
        private DevExpress.XtraEditors.CheckEdit pcCheckEdit;
        private DevExpress.XtraEditors.CheckEdit psCheckEdit;
        private DevExpress.XtraEditors.SimpleButton hsButton;
        private DevExpress.XtraEditors.CheckEdit hsCheckEdit;
        private DevExpress.XtraEditors.SimpleButton boButton;
        private System.Windows.Forms.RichTextBox richTextBox;
        private System.IO.Ports.SerialPort serialPort4;
        private DevExpress.XtraEditors.SimpleButton ssButton;
        private DevExpress.XtraEditors.CheckEdit ssCheckEdit;
        private DevExpress.XtraEditors.SimpleButton clearButton;
        private DevExpress.XtraEditors.SimpleButton startButton;
        private DevExpress.XtraEditors.LabelControl labelControl;
    }
}
