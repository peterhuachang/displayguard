﻿namespace DisplayGuardServer
{
    partial class DisplayGuardServerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.displayGuardEmulator1 = new DisplayGuardServer.DisplayGuardEmulator();
            this.SuspendLayout();
            // 
            // displayGuardEmulator1
            // 
            this.displayGuardEmulator1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.displayGuardEmulator1.Location = new System.Drawing.Point(0, 0);
            this.displayGuardEmulator1.Name = "displayGuardEmulator1";
            this.displayGuardEmulator1.Size = new System.Drawing.Size(982, 553);
            this.displayGuardEmulator1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 553);
            this.Controls.Add(this.displayGuardEmulator1);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Test";
            this.ResumeLayout(false);

        }

        #endregion

        private DisplayGuardEmulator displayGuardEmulator1;

    }
}