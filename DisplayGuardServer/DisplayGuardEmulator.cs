﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.IO.Ports;
using System.Threading;

namespace DisplayGuardServer
{
    public partial class DisplayGuardEmulator : DevExpress.XtraEditors.XtraUserControl
    {
        public SerialPort SerialPort { get { return this.serialPort4; } }

        private BoxOpen boxOpen;

        public DisplayGuardEmulator()
        {          
            InitializeComponent();
            ButtonEnabled(false);
        }

        private void SerialPort4DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            string response = this.serialPort4.ReadExisting();

            if("BA".Equals(response))
            {
                richTextBox.Invoke(new EventHandler(delegate
                {
                    richTextBox.AppendText("Client> " + response + "                    " + DateTime.Now + "\n");
                }));  
                this.boxOpen.Stop();   
            }
            else
            {
                richTextBox.Invoke(new EventHandler(delegate
                {
                    richTextBox.AppendText("Client> " + response + "                    " + DateTime.Now + "\n");
                }));  
            }         
        }

        private void OnPowerOpen(object sender, EventArgs e)
        {
            if(this.poCheckEdit.Checked)
            {
                new Thread(new ThreadStart(PO)).Start();
            }
            else
            {
                this.serialPort4.Write("POF");
            }
        }

        private void OnHDMIOpen(object sender, EventArgs e)
        {
            if(this.hoCheckEdit.Checked)
            {
                new Thread(new ThreadStart(SwitchHDMI)).Start();
            }
            else
            {
                this.serialPort4.Write("HOF");
            }
        }

        private void OnPowerClose(object sender, EventArgs e)
        {
            if (this.pcCheckEdit.Checked)
            {
                new Thread(new ThreadStart(PC)).Start();
            }
            else
            {
                this.serialPort4.Write("PCF");
            }
        }

        private void PO()
        {
            this.serialPort4.Write("POF");
            System.Threading.Thread.Sleep(2000);
            this.serialPort4.Write("POF");
            System.Threading.Thread.Sleep(2000);
            this.serialPort4.Write("POS");
        }

        private void PC()
        {
            this.serialPort4.Write("PCF");
            System.Threading.Thread.Sleep(2000);
            this.serialPort4.Write("PCF");
            System.Threading.Thread.Sleep(2000);
            this.serialPort4.Write("PCS");
        }

        private void SwitchHDMI()
        {
            this.serialPort4.Write("HOF");
            System.Threading.Thread.Sleep(2000);
            this.serialPort4.Write("HOF");
            System.Threading.Thread.Sleep(2000);
            this.serialPort4.Write("HOS");
        }

        private void OnRequestPowerState(object sender, EventArgs e)
        {
            if (this.psCheckEdit.Checked)
            {
                this.serialPort4.Write("PSON");
            }
            else
            {
                this.serialPort4.Write("PSOFF");
            }
        }

        private void OnRequestHDMIState(object sender, EventArgs e)
        {
            if (this.hsCheckEdit.Checked)
            {
                this.serialPort4.Write("HSON");
            }
            else
            {
                this.serialPort4.Write("HSOFF");
            }
        }

        private void OnBoxOpen(object sender, EventArgs e)
        {
            this.boxOpen.Start();
        }

        private void OnStopSignal(object sender, EventArgs e)
        {
            if(this.ssCheckEdit.Checked)
            {
                this.serialPort4.Write("SSS");
            }
            else
            {
                this.serialPort4.Write("SSF");
            }
        }

        private void OnClear(object sender, EventArgs e)
        {
            this.richTextBox.Clear();
        }

        private void OnStart(object sender, EventArgs e)
        {
            this.serialPort4.DataReceived += new SerialDataReceivedEventHandler(SerialPort4DataReceived);
            try
            {
                this.serialPort4.Open();
                this.labelControl.Text = "成功";
                ButtonEnabled(true);
            }
            catch(Exception ex)
            {
                this.labelControl.Text = ex.Message;
                ButtonEnabled(false);
            }
            
            this.boxOpen = new BoxOpen(this);
        }

        private void ButtonEnabled(bool enabled)
        {
            this.poButton.Enabled = enabled;
            this.pcButton.Enabled = enabled;
            this.psButton.Enabled = enabled;
            this.hoButton.Enabled = enabled;
            this.hsButton.Enabled = enabled;
            this.boButton.Enabled = enabled;
            this.ssButton.Enabled = enabled;
        }
    }

    public class BoxOpen
    {
        private bool started;

        private DisplayGuardEmulator obj;

        public BoxOpen(DisplayGuardEmulator obj)
        {
            this.obj = obj;
        }

        public void Start()
        {
            if (this.started)
            {
                return;
            }
            this.started = true;
            new Thread(new ThreadStart(Running)).Start();
        }

        public void Stop()
        {
            this.started = false;
            lock (this)
            {
                Monitor.PulseAll(this);
            }
        }

        public void Running()
        {
            while (this.started)
            {
                lock (this)
                {
                    this.obj.SerialPort.Write("BO");
                    Monitor.Wait(this, 1000);
                }
            }
        }

    }
 }
