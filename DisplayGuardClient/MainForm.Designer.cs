﻿namespace DisplayGuardClient
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.connectButton = new DevExpress.XtraEditors.SimpleButton();
            this.disconnectButton = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.displayBox = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comBox = new DevExpress.XtraEditors.ComboBoxEdit();
            this.psButton = new DevExpress.XtraEditors.SimpleButton();
            this.hsButton = new DevExpress.XtraEditors.SimpleButton();
            this.poButton = new DevExpress.XtraEditors.SimpleButton();
            this.pcButton = new DevExpress.XtraEditors.SimpleButton();
            this.hoButton = new DevExpress.XtraEditors.SimpleButton();
            this.baButton = new DevExpress.XtraEditors.SimpleButton();
            this.psEdit = new DevExpress.XtraEditors.TextEdit();
            this.hsEdit = new DevExpress.XtraEditors.TextEdit();
            this.timeEdit = new DevExpress.XtraEditors.TextEdit();
            this.richTextBox = new System.Windows.Forms.RichTextBox();
            this.connectResultLabel = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.ackOpenTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.disconnectLabel = new DevExpress.XtraEditors.LabelControl();
            this.clearButton = new DevExpress.XtraEditors.SimpleButton();
            this.ssButton = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.displayBox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comBox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.psEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hsEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ackOpenTextEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // connectButton
            // 
            this.connectButton.Appearance.Font = new System.Drawing.Font("Microsoft JhengHei", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectButton.Appearance.Options.UseFont = true;
            this.connectButton.Location = new System.Drawing.Point(348, 27);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(75, 30);
            this.connectButton.TabIndex = 0;
            this.connectButton.Text = "連線";
            this.connectButton.Click += new System.EventHandler(this.OnConnect);
            // 
            // disconnectButton
            // 
            this.disconnectButton.Appearance.Font = new System.Drawing.Font("Microsoft JhengHei", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.disconnectButton.Appearance.Options.UseFont = true;
            this.disconnectButton.Enabled = false;
            this.disconnectButton.Location = new System.Drawing.Point(500, 27);
            this.disconnectButton.Name = "disconnectButton";
            this.disconnectButton.Size = new System.Drawing.Size(75, 30);
            this.disconnectButton.TabIndex = 1;
            this.disconnectButton.Text = "斷線";
            this.disconnectButton.Click += new System.EventHandler(this.OnDisconnect);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Microsoft JhengHei", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(37, 25);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(82, 32);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "電視型號 :";
            // 
            // displayBox
            // 
            this.displayBox.EditValue = "D1";
            this.displayBox.Location = new System.Drawing.Point(125, 29);
            this.displayBox.Name = "displayBox";
            this.displayBox.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.displayBox.Properties.Items.AddRange(new object[] {
            "D1",
            "D2"});
            this.displayBox.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.displayBox.Size = new System.Drawing.Size(88, 24);
            this.displayBox.TabIndex = 4;
            // 
            // comBox
            // 
            this.comBox.EditValue = "COM3";
            this.comBox.Location = new System.Drawing.Point(228, 29);
            this.comBox.Name = "comBox";
            this.comBox.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comBox.Properties.Items.AddRange(new object[] {
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8",
            "COM9"});
            this.comBox.Size = new System.Drawing.Size(100, 24);
            this.comBox.TabIndex = 5;
            // 
            // psButton
            // 
            this.psButton.Appearance.Font = new System.Drawing.Font("Microsoft JhengHei", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.psButton.Appearance.Options.UseFont = true;
            this.psButton.Enabled = false;
            this.psButton.Location = new System.Drawing.Point(24, 91);
            this.psButton.Name = "psButton";
            this.psButton.Size = new System.Drawing.Size(189, 30);
            this.psButton.TabIndex = 6;
            this.psButton.Text = "取得電源目前狀態";
            this.psButton.Click += new System.EventHandler(this.OnRequestPowerState);
            // 
            // hsButton
            // 
            this.hsButton.Appearance.Font = new System.Drawing.Font("Microsoft JhengHei", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hsButton.Appearance.Options.UseFont = true;
            this.hsButton.Enabled = false;
            this.hsButton.Location = new System.Drawing.Point(24, 139);
            this.hsButton.Name = "hsButton";
            this.hsButton.Size = new System.Drawing.Size(189, 30);
            this.hsButton.TabIndex = 7;
            this.hsButton.Text = "取得HDMI目前狀態";
            this.hsButton.Click += new System.EventHandler(this.OnRequestHDMIState);
            // 
            // poButton
            // 
            this.poButton.Appearance.Font = new System.Drawing.Font("Microsoft JhengHei", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.poButton.Appearance.Options.UseFont = true;
            this.poButton.Enabled = false;
            this.poButton.Location = new System.Drawing.Point(24, 196);
            this.poButton.Name = "poButton";
            this.poButton.Size = new System.Drawing.Size(189, 30);
            this.poButton.TabIndex = 8;
            this.poButton.Text = "開電視";
            this.poButton.Click += new System.EventHandler(this.OnPowerOpen);
            // 
            // pcButton
            // 
            this.pcButton.Appearance.Font = new System.Drawing.Font("Microsoft JhengHei", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pcButton.Appearance.Options.UseFont = true;
            this.pcButton.Enabled = false;
            this.pcButton.Location = new System.Drawing.Point(24, 245);
            this.pcButton.Name = "pcButton";
            this.pcButton.Size = new System.Drawing.Size(189, 30);
            this.pcButton.TabIndex = 9;
            this.pcButton.Text = "關電視";
            this.pcButton.Click += new System.EventHandler(this.OnPowerClose);
            // 
            // hoButton
            // 
            this.hoButton.Appearance.Font = new System.Drawing.Font("Microsoft JhengHei", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hoButton.Appearance.Options.UseFont = true;
            this.hoButton.Enabled = false;
            this.hoButton.Location = new System.Drawing.Point(24, 303);
            this.hoButton.Name = "hoButton";
            this.hoButton.Size = new System.Drawing.Size(189, 30);
            this.hoButton.TabIndex = 10;
            this.hoButton.Text = "開啟HDMI";
            this.hoButton.Click += new System.EventHandler(this.OnSwitchHDMI);
            // 
            // baButton
            // 
            this.baButton.Appearance.Font = new System.Drawing.Font("Microsoft JhengHei", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.baButton.Appearance.Options.UseFont = true;
            this.baButton.Enabled = false;
            this.baButton.Location = new System.Drawing.Point(24, 396);
            this.baButton.Name = "baButton";
            this.baButton.Size = new System.Drawing.Size(189, 30);
            this.baButton.TabIndex = 11;
            this.baButton.Text = "警報確認";
            this.baButton.Click += new System.EventHandler(this.OnAlarmCheck);
            // 
            // psEdit
            // 
            this.psEdit.Location = new System.Drawing.Point(228, 91);
            this.psEdit.Name = "psEdit";
            this.psEdit.Properties.AllowFocused = false;
            this.psEdit.Properties.AutoHeight = false;
            this.psEdit.Properties.ReadOnly = true;
            this.psEdit.Size = new System.Drawing.Size(100, 30);
            this.psEdit.TabIndex = 12;
            // 
            // hsEdit
            // 
            this.hsEdit.Location = new System.Drawing.Point(228, 139);
            this.hsEdit.Name = "hsEdit";
            this.hsEdit.Properties.AutoHeight = false;
            this.hsEdit.Properties.ReadOnly = true;
            this.hsEdit.Size = new System.Drawing.Size(100, 30);
            this.hsEdit.TabIndex = 13;
            // 
            // timeEdit
            // 
            this.timeEdit.Location = new System.Drawing.Point(670, 447);
            this.timeEdit.Name = "timeEdit";
            this.timeEdit.Properties.Appearance.BackColor = System.Drawing.Color.Lime;
            this.timeEdit.Properties.Appearance.Font = new System.Drawing.Font("Microsoft JhengHei", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeEdit.Properties.Appearance.Options.UseBackColor = true;
            this.timeEdit.Properties.Appearance.Options.UseFont = true;
            this.timeEdit.Properties.ReadOnly = true;
            this.timeEdit.Size = new System.Drawing.Size(182, 28);
            this.timeEdit.TabIndex = 14;
            // 
            // richTextBox
            // 
            this.richTextBox.Location = new System.Drawing.Point(348, 91);
            this.richTextBox.Name = "richTextBox";
            this.richTextBox.ReadOnly = true;
            this.richTextBox.Size = new System.Drawing.Size(504, 335);
            this.richTextBox.TabIndex = 15;
            this.richTextBox.Text = "";
            // 
            // connectResultLabel
            // 
            this.connectResultLabel.Appearance.Font = new System.Drawing.Font("Microsoft JhengHei", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectResultLabel.Location = new System.Drawing.Point(429, 32);
            this.connectResultLabel.Name = "connectResultLabel";
            this.connectResultLabel.Size = new System.Drawing.Size(34, 22);
            this.connectResultLabel.TabIndex = 16;
            this.connectResultLabel.Text = "未知";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Microsoft JhengHei", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(535, 450);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(114, 22);
            this.labelControl2.TabIndex = 17;
            this.labelControl2.Text = "最後更新時間 : ";
            // 
            // ackOpenTextEdit
            // 
            this.ackOpenTextEdit.Location = new System.Drawing.Point(228, 396);
            this.ackOpenTextEdit.Name = "ackOpenTextEdit";
            this.ackOpenTextEdit.Properties.AutoHeight = false;
            this.ackOpenTextEdit.Properties.ReadOnly = true;
            this.ackOpenTextEdit.Size = new System.Drawing.Size(114, 30);
            this.ackOpenTextEdit.TabIndex = 21;
            // 
            // disconnectLabel
            // 
            this.disconnectLabel.Appearance.Font = new System.Drawing.Font("Microsoft JhengHei", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.disconnectLabel.Location = new System.Drawing.Point(581, 32);
            this.disconnectLabel.Name = "disconnectLabel";
            this.disconnectLabel.Size = new System.Drawing.Size(34, 22);
            this.disconnectLabel.TabIndex = 22;
            this.disconnectLabel.Text = "未知";
            // 
            // clearButton
            // 
            this.clearButton.Appearance.Font = new System.Drawing.Font("Microsoft JhengHei", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearButton.Appearance.Options.UseFont = true;
            this.clearButton.Location = new System.Drawing.Point(762, 55);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(90, 30);
            this.clearButton.TabIndex = 23;
            this.clearButton.Text = "清除畫面";
            this.clearButton.Click += new System.EventHandler(this.OnClear);
            // 
            // ssButton
            // 
            this.ssButton.Appearance.Font = new System.Drawing.Font("Microsoft JhengHei", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ssButton.Appearance.Options.UseFont = true;
            this.ssButton.Location = new System.Drawing.Point(24, 349);
            this.ssButton.Name = "ssButton";
            this.ssButton.Size = new System.Drawing.Size(189, 30);
            this.ssButton.TabIndex = 24;
            this.ssButton.Text = "停止發送訊號";
            this.ssButton.Click += new System.EventHandler(this.OnStopSignal);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(866, 491);
            this.Controls.Add(this.ssButton);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.disconnectLabel);
            this.Controls.Add(this.ackOpenTextEdit);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.connectResultLabel);
            this.Controls.Add(this.richTextBox);
            this.Controls.Add(this.timeEdit);
            this.Controls.Add(this.hsEdit);
            this.Controls.Add(this.psEdit);
            this.Controls.Add(this.baButton);
            this.Controls.Add(this.hoButton);
            this.Controls.Add(this.pcButton);
            this.Controls.Add(this.poButton);
            this.Controls.Add(this.hsButton);
            this.Controls.Add(this.psButton);
            this.Controls.Add(this.comBox);
            this.Controls.Add(this.displayBox);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.disconnectButton);
            this.Controls.Add(this.connectButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "模擬器";
            ((System.ComponentModel.ISupportInitialize)(this.displayBox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comBox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.psEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hsEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ackOpenTextEdit.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton connectButton;
        private DevExpress.XtraEditors.SimpleButton disconnectButton;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit displayBox;
        private DevExpress.XtraEditors.ComboBoxEdit comBox;
        private DevExpress.XtraEditors.SimpleButton psButton;
        private DevExpress.XtraEditors.SimpleButton hsButton;
        private DevExpress.XtraEditors.SimpleButton poButton;
        private DevExpress.XtraEditors.SimpleButton pcButton;
        private DevExpress.XtraEditors.SimpleButton hoButton;
        private DevExpress.XtraEditors.SimpleButton baButton;
        private DevExpress.XtraEditors.TextEdit psEdit;
        private DevExpress.XtraEditors.TextEdit hsEdit;
        private DevExpress.XtraEditors.TextEdit timeEdit;
        private System.Windows.Forms.RichTextBox richTextBox;
        private DevExpress.XtraEditors.LabelControl connectResultLabel;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit ackOpenTextEdit;
        private DevExpress.XtraEditors.LabelControl disconnectLabel;
        private DevExpress.XtraEditors.SimpleButton clearButton;
        private DevExpress.XtraEditors.SimpleButton ssButton;
    }
}

