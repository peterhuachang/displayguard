﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using DisplayGuard;


namespace DisplayGuardClient
{
    public partial class MainForm : Form
    {
        private GuardManager mgr;

        public MainForm()
        {
            InitializeComponent();

            FunctionButtonEnabled(false);           
        }
        #region Resgister Event
        void Error(GuardManager source, GuardManagerEventArgs args)
        {
            richTextBox.Invoke(new EventHandler(delegate
            {
                richTextBox.AppendText("Power Open or Close Error  "+ DateTime.Now +"\n");
                this.timeEdit.Text = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
            }));  
        }

        void BoxOpen(GuardManager source, GuardManagerEventArgs args)
        {
            richTextBox.Invoke(new EventHandler(delegate
            {
                richTextBox.AppendText("Box Open  " + DateTime.Now +"\n");
                this.timeEdit.Text = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
            }));  
        }

        //Signal事件
        void OnSignal(GuardManager source, GuardManagerEventArgs args)
        {
            string result = args.Equals(GuardManagerEventArgs.SUCCESS) ? "成功" : "失敗";
            richTextBox.Invoke(new EventHandler(delegate
            {
                richTextBox.AppendText("Stop Signal指令  " + result + "       " + DateTime.Now + "\n");
                this.timeEdit.Text = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
            }));
        }

        //HDMI事件
        void OnSwitchHDMI(GuardManager source, GuardManagerEventArgs args)
        {
            string result = this.mgr.HDMIState.Equals(HDMIStateType.ON) ? "ON" : "OFF";
            richTextBox.Invoke(new EventHandler(delegate
            {
                richTextBox.AppendText("HDMI狀態  " + result + "       " + DateTime.Now + "\n");
                this.timeEdit.Text = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
                this.hsEdit.Text = result;
            }));
        }

        //Power事件
        void OnPower(GuardManager source, GuardManagerEventArgs args)
        {
            string result = this.mgr.PowerState.Equals(PowerStateType.ON) ? "ON" : "OFF";
            richTextBox.Invoke(new EventHandler(delegate
            {
                richTextBox.AppendText("電視機狀態  " + result + "       " + DateTime.Now + "\n");
                this.timeEdit.Text = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
                this.psEdit.Text = result;
            }));

        }
        #endregion

        #region Event
        //連線
        private void OnConnect(object sender, EventArgs e)
        {        
            this.mgr = new GuardManager(this.comBox.Text);
            this.mgr.Open += BoxOpen;
            this.mgr.Error += Error;
            this.mgr.Power += OnPower;
            this.mgr.HDMI += OnSwitchHDMI;
            this.mgr.Signal += OnSignal;
            this.mgr.DisplayType = "D1".Equals(this.displayBox.Text) ? DisplayType.D1 : DisplayType.D2;
            if(this.mgr.TryStart())
            {
                this.connectResultLabel.Text = "成功";
                this.disconnectLabel.Text = "";
                this.displayBox.Enabled = false;
                this.comBox.Enabled = false;
                this.connectButton.Enabled = false;
                FunctionButtonEnabled(true);
                this.timeEdit.Text = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
                this.mgr.StopSignal();
            }
            else
            {
                this.connectResultLabel.Text = "失敗";
                this.disconnectLabel.Text = "";
                FunctionButtonEnabled(false);
                this.timeEdit.Text = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
            }
        }

        //斷線
        private void OnDisconnect(object sender, EventArgs e)
        {
            if(this.mgr.Stop())
            {
                this.connectButton.Enabled = true;
                this.displayBox.Enabled = true;
                this.comBox.Enabled = true;
                this.psEdit.Text = "";
                this.hsEdit.Text = "";
                this.disconnectLabel.Text = "成功";
                this.connectResultLabel.Text = "";
                this.ackOpenTextEdit.Text = "";
                this.ackOpenTextEdit.BackColor = Color.FromArgb(245, 245, 247);
                FunctionButtonEnabled(false);
                this.timeEdit.Text = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
            }
            else
            {
                FunctionButtonEnabled(true);
                this.disconnectLabel.Text = "失敗";
                this.connectResultLabel.Text = "";
                this.timeEdit.Text = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
            }
        }

        //取得電源目前狀態
        private void OnRequestPowerState(object sender, EventArgs e)
        {
            if(!this.mgr.RequestPowerState())
            {
                this.psEdit.Text = "錯誤";
                this.psEdit.BackColor = Color.Red;
                this.timeEdit.Text = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
            }
        }

        //取得HDMI目前狀態
        private void OnRequestHDMIState(object sender, EventArgs e)
        {
            if (!this.mgr.RequestHDMIState())
            {
                this.timeEdit.Text = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
                this.hsEdit.BackColor = Color.Red;
                this.hsEdit.Text = "錯誤";
            }
        }

        private void OnAlarmCheck(object sender, EventArgs e)
        {
            if (this.mgr.AckOpen())
            {
                this.ackOpenTextEdit.Text = "指令傳送成功";
                this.ackOpenTextEdit.BackColor = Color.Lime;
                this.timeEdit.Text = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
            }
            else
            {
                this.ackOpenTextEdit.Text = "指令傳送失敗";
                this.ackOpenTextEdit.BackColor = Color.Red;
                this.timeEdit.Text = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
            }
        }

        //開電視
        private void OnPowerOpen(object sender, EventArgs e)
        {
            if (this.mgr.PowerTurnOn())
            {               
                this.timeEdit.Text = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
            }
            else
            {              
                this.timeEdit.Text = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
            }
        }

        //關電視
        private void OnPowerClose(object sender, EventArgs e)
        {
            if (this.mgr.PowerTurnOff())
            {            
                this.timeEdit.Text = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
            }
            else
            {
                this.timeEdit.Text = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
            }
        }

        private void OnSwitchHDMI(object sender, EventArgs e)
        {
            if (this.mgr.SwitchHDMI())
            {             
                this.timeEdit.Text = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
            }
            else
            {               
                this.timeEdit.Text = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
            }
        }

        //停止傳送訊號
        private void OnStopSignal(object sender, EventArgs e)
        {
            if (!this.mgr.StopSignal())
            {
                richTextBox.Invoke(new EventHandler(delegate
                {
                    richTextBox.AppendText("傳送StopSignal指令失敗" + "       " + DateTime.Now + "\n");
                    this.timeEdit.Text = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss");
                }));
            }
        }     
        #endregion 

        private void FunctionButtonEnabled(bool enabled)
        {
            this.psButton.Enabled = enabled;
            this.hsButton.Enabled = enabled;
            this.poButton.Enabled = enabled;
            this.pcButton.Enabled = enabled;
            this.hoButton.Enabled = enabled;
            this.baButton.Enabled = enabled;
            this.ssButton.Enabled = enabled;
            this.disconnectButton.Enabled = enabled;
        }

        //清除log畫面
        private void OnClear(object sender, EventArgs e)
        {
            this.richTextBox.Clear();
        }


        
    }
}
