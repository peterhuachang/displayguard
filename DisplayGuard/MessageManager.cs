﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIA.Comm;

namespace DisplayGuard
{
    public class MessageManager : IMessageManager
    {
        private List<string> callIns;

        public MessageManager()
        {
            this.callIns = new List<string>();
            this.callIns.Add("BO");
            this.callIns.Add("POS");
            this.callIns.Add("POF");
            this.callIns.Add("PSON");
            this.callIns.Add("PSOFF");
            this.callIns.Add("PCS");
            this.callIns.Add("PCF");
            this.callIns.Add("HOS");
            this.callIns.Add("HOF");
            this.callIns.Add("HSON");
            this.callIns.Add("HSOFF");
            this.callIns.Add("SSS");
            this.callIns.Add("SSF");
            this.callIns.Add("D1S");
            this.callIns.Add("DF");
            this.callIns.Add("D2S");
            this.callIns.Add("PCE");
            this.callIns.Add("POE");
        }

        public string FindTx(byte[] data)
        {
            return Encoding.UTF8.GetString(data);
        }

        public bool IsCallIn(string cmd)
        {
            return this.callIns.Contains(cmd);
        }

        public bool TryFindCmd(byte[] data, out string cmd)
        {
            /*if (data.Length < 0 || data.Length>2)
            {
                cmd = null;
                return false;
            }
            */
            cmd = Encoding.UTF8.GetString(data);
            System.Console.WriteLine(cmd);
            return true;
        }
    }
}
