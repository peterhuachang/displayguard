﻿using log4net;
using DisplayGuard.Command;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UIA.Comm;
using UIA.Comm.Protocol.NG;



namespace DisplayGuard
{   

    public delegate void GuardManagerEventHandler(GuardManager source, GuardManagerEventArgs args);

    #region enum
    public enum GuardManagerEventArgs
    {
        ON,
        OFF,
        ALARM,
        ERROR,
        SUCCESS,
        FAIL
    }

    public enum PowerStateType      //電源目前狀態
    {
        ON,
        OFF
    }

    public enum HDMIStateType       // HDMI 目前狀態
    {
        ON,
        OFF
    }

    public enum DisplayStateType
    {

    }

    public enum DisplayType
    {
        D1,
        D2
    }
    #endregion

    public class GuardManager
    {
        public event GuardManagerEventHandler Open;

        public event GuardManagerEventHandler Power;    //電源目前狀態

        public event GuardManagerEventHandler HDMI;     //HDMI 狀態

        public event GuardManagerEventHandler Error;

        public event GuardManagerEventHandler Display;

        public event GuardManagerEventHandler Signal;

        private SerialPort com3;

        private SerialClient serialClient;

        private GuardManagerCheck guardCheck;

        public PowerStateType PowerState { get; set; }//電源狀態 

        public HDMIStateType HDMIState { get; set; }//hdmi 狀態

        public DisplayType DisplayType { get; set;  }

        private static readonly ILog logger = LogManager.GetLogger(typeof(GuardManager));

        public GuardManager(string comName = "COM3")
        {           
            PowerState = PowerStateType.OFF;
            HDMIState = HDMIStateType.OFF;
            
            this.com3 = new SerialPort(comName, 9600);
            this.guardCheck = new GuardManagerCheck(this);
            this.serialClient = new SerialClient(this.com3, new NGProtocol<SerialClient>(), new MessageManager(), "Client");
            RegisterCallIn();
        }

        #region Commands
        public bool PowerTurnOn()
        {
            try
            {
                if (TryStart())
                {
                    this.serialClient.Send(Encoding.UTF8.GetBytes("PO"));
                    logger.Debug("GuardManager > Power open");
                    return true;
                }
                else
                {
                    logger.Debug("GuardManager > Power open failure...");
                    return false;
                }
            }
            catch
            {
                logger.Debug("GuardManager > Power open failure...");
                return false;
            }        
        }

        public bool PowerTurnOff()
        {
            try
            {
                if (TryStart())
                {
                    this.serialClient.Send(Encoding.UTF8.GetBytes("PC"));
                    logger.Debug("GuardManager > Power close");
                    return true;
                }
                else
                {
                    logger.Debug("GuardManager > Power close failure...");
                    return false;
                }
            }
            catch
            {
                logger.Debug("GuardManager > Power close failure...");
                return false;
            }        
        }

        public bool RequestPowerState()
        {
            try
            {
                if (TryStart())
                {
                    this.serialClient.Send(Encoding.UTF8.GetBytes("PS"));
                    logger.Debug("GuardManager > RequestPowerState");
                    return true;
                }
                else
                {
                    logger.Debug("GuardManager > RequestPowerState failure..");
                    return false;
                }
            }
            catch
            {
                logger.Debug("GuardManager > RequestPowerState failure..");
                return false;
            }         
        }

        public bool SwitchHDMI()
        {
            try
            {
                if (TryStart())
                {
                    this.serialClient.Send(Encoding.UTF8.GetBytes("HO"));
                    logger.Debug("GuardManager > Switch HDMI");
                    return true;
                }
                else
                {
                    logger.Debug("GuardManager > Switch HDMI failure...");
                    return false;
                }
            }
            catch
            {
                logger.Debug("GuardManager > Switch HDMI failure...");
                return false;
            }         
        }

        public bool RequestHDMIState()
        {
            try
            {
                if (TryStart())
                {
                    this.serialClient.Send(Encoding.UTF8.GetBytes("HS"));
                    logger.Debug("GuardManager > RequestHDMIState");
                    return true;
                }
                else
                {
                    logger.Debug("GuardManager > RequestHDMIState failure..");
                    return false;
                }
            }
            catch
            {
                logger.Debug("GuardManager > RequestHDMIState failure..");
                return false;
            }         
        }

        public bool StopSignal()
        {
            try
            {
                if (TryStart())
                {
                    this.serialClient.Send(Encoding.UTF8.GetBytes("SS"));
                    logger.Debug("GuardManager > Stop Signal");
                    return true;
                }
                else
                {
                    logger.Debug("GuardManager > Stop Signal failure...");
                    return false;
                }
            }
            catch
            {
                logger.Debug("GuardManager > Stop Signal failure...");
                return false;
            }        
        }

        public bool AckOpen()
        {
            try
            {
                if (TryStart())
                {
                    this.serialClient.Send(Encoding.UTF8.GetBytes("BA"));
                    logger.Debug("GuardManager > Ack Box open");
                    return true;
                }
                else
                {
                    logger.Debug("GuardManager > Ack Box open failure...");
                    return false;
                }
            }
            catch
            {
                logger.Debug("GuardManager > Ack Box open failure...");
                return false;
            }        
        }
        #endregion

        public bool TryStart()
        {
            try
            {
                if(this.com3.IsOpen)
                {
                    return true;
                }

                this.serialClient.Start();
                switch (DisplayType)
                {
                    case DisplayType.D1:
                        this.serialClient.Send(Encoding.UTF8.GetBytes("D1"));
                        break;
                    case DisplayType.D2:
                        this.serialClient.Send(Encoding.UTF8.GetBytes("D2"));
                        break;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Stop()
        {
            try
            {
                if (TryStart())
                {
                    this.serialClient.Stop();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }         
        }

        public void GuardManagerCheck()
        {                   
              this.guardCheck.Start();                      
        }

        #region Handle Event
        public void RaiseOpen(GuardManagerEventArgs args)
        {
            if(Open!=null)
            {
                Open(this,args);
            }
        }

        public void RaiseError(GuardManagerEventArgs args)
        {
            if (Error != null)
            {
                Error(this, args);
            }
        }

        public void RaiseDisplay(GuardManagerEventArgs args)
        {
            if(Display!=null)
            {
                Display(this, args);
            }
        }

        public void RaisePowerEvent(GuardManagerEventArgs args)//電源開機
        {
            PowerStateType prev = PowerState;
            PowerStateType curr = args == GuardManagerEventArgs.ON ? PowerStateType.ON : PowerStateType.OFF;
           /* if(prev == curr)
            {
                return;
            }*/

            PowerState = curr;
            if(Power != null)
            {
                Power(this, args);
            }
        }

        public void RaiseHdmiEvent(GuardManagerEventArgs args)
        {
            HDMIStateType prev = HDMIState;
            HDMIStateType curr = args == GuardManagerEventArgs.ON ? HDMIStateType.ON : HDMIStateType.OFF;
           /* if (prev == curr)
            {
                return;
            }*/

            HDMIState = curr;
            if (HDMI != null)
            {
                HDMI(this, args);
            }
        }

        public void RaiseSignal(GuardManagerEventArgs args)
        {
            if(Signal!=null)
            {
                Signal(this, args);
            }
        }

        #endregion
        private void RegisterCallIn()
        {
            this.serialClient.RegisterCallIn(new BoxOpenCommand(this));
            this.serialClient.RegisterCallIn(new PowerOnSuccessCommand(this));
            this.serialClient.RegisterCallIn(new PowerOnFailCommand(this));
            this.serialClient.RegisterCallIn(new PowerStateOnCommand(this));
            this.serialClient.RegisterCallIn(new PowerStateOffCommand(this));
            this.serialClient.RegisterCallIn(new HDMIOnSuccessCommand(this));
            this.serialClient.RegisterCallIn(new HDMIOnFailCommand(this));
            this.serialClient.RegisterCallIn(new HDMIStateOnCommand(this));
            this.serialClient.RegisterCallIn(new HDMIStateOffCommand(this));
            this.serialClient.RegisterCallIn(new PowerCloseSuccessCommand(this));
            this.serialClient.RegisterCallIn(new PowerCloseFailCommand(this));
            this.serialClient.RegisterCallIn(new StopSignalSuccess(this));
            this.serialClient.RegisterCallIn(new StopSignalFail(this));
            this.serialClient.RegisterCallIn(new D1SuccessCommand(this));
            this.serialClient.RegisterCallIn(new D2SuccessCommand(this));
            this.serialClient.RegisterCallIn(new DisplayFailCommand(this));
            this.serialClient.RegisterCallIn(new PowerOnErrorCommand(this));
            this.serialClient.RegisterCallIn(new PowerCloseErrorCommand(this));
        }
    }

    public class GuardManagerCheck
    {
        private bool started;

        private GuardManager obj; 

        public GuardManagerCheck(GuardManager obj)
        {
            this.obj = obj;
        }

        public void Start()
        {
            if(this.started)
            {
                return;
            }
            this.started = true;
            new Thread(new ThreadStart(Running)).Start();
        }

        public void Stop()
        {
            this.started = false;
            lock(this.obj)
            {
                Monitor.PulseAll(this);
            }
        }

        public void Running()
        {
            while(this.started)
            {
                lock(this)
                {
                    this.obj.RequestPowerState();
                    System.Threading.Thread.Sleep(3000);
                    this.obj.RequestHDMIState();
                    Monitor.Wait(this,60000);
                }
            }
        }

    }
} 
