﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace DisplayGuard.Command
{
    public class TurnOffCommand : ICommand
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(TurnOffCommand));

        public const string NAME = "TURNOFF";

        public bool Execute()
        {
            try
            {
                //QFT.MonitorControl.IRControl.WDHIR8320Control ir = new MonitorControl.IRControl.WDHIR8320Control("COM1");
                //if (ir.Connect())
                //{
                //    ir.SendPowerControlCommand();
                //    ir.Disconnect();
                //}
                //else
                //{
                //    logger.Warn("TurnOff> Connect IR failure");
                //}
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return true;
        }
    }
}
