﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIA.Comm;

namespace DisplayGuard.Command
{
    public class PowerOnFailCommand : IMessageCallIn<SerialClient>
    {
        private GuardManager guard;

        public PowerOnFailCommand(GuardManager guard)
        {
            this.guard = guard;
        }

        public string CmdName
        {
            get { return"POF"; }//Power On Fail
        }

        public void Execute(SerialClient controller, byte[] request)
        {
            Console.WriteLine(CmdName + "> " + Encoding.UTF8.GetString(request));
            this.guard.RaisePowerEvent(GuardManagerEventArgs.OFF);
        }
    }
}
