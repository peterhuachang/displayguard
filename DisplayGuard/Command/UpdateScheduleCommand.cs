﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Xml;

using log4net;

namespace DisplayGuard.Command
{
    /// <summary>
    /// Uupdate schedule.
    /// </summary>
    public class UpdateScheduleCommand : ICommand
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(UpdateScheduleCommand));

        public const string NAME = "UPDSCH";

        public string SchDir { get; set; }

        public DateTime SchDate { get; set; }

        public string SchXml { get; set; }

        public UpdateScheduleCommand()
        {
        }

        public bool Execute()
        {
            if (string.IsNullOrEmpty(SchXml))
            {
                logger.Info("UpdateSchedule> nodata");
                return false;
            }

            DateTime now = DateTime.Now;
            if (now.Year != SchDate.Year || now.Month != SchDate.Month || now.Day != SchDate.Day)
            {
                logger.WarnFormat("UpdateSchedule> not today data!");
            }

            try
            {
                logger.Info("UpdateSchedule>");
                logger.Info(SchXml);

                string localFile = string.Format("{0}schedule-{1:yyyyMMdd}.xml", SchDir, SchDate);
                string bakFile = string.Format("{0}schedule-{1:yyyyMMdd_HHmmss}.xml", SchDir, SchDate);
                if (File.Exists(localFile))
                {
                    logger.InfoFormat("UpdateSchedule> backup file: {0:s}", bakFile);
                    File.Copy(localFile, bakFile);
                }
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(SchXml);
                xdoc.Save(localFile);
                logger.InfoFormat("UpdateSchedule> schedule file: {0:s}", localFile);
                return true;
            }
            catch (Exception ex)
            {
                logger.Error("UpdateSchedule> failure", ex);
                return false;
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
