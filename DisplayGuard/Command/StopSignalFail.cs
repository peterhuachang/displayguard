﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIA.Comm;

namespace DisplayGuard.Command
{
    public class StopSignalFail : IMessageCallIn<SerialClient>
    {
        private GuardManager guard;

        public StopSignalFail(GuardManager guard)
        {
            this.guard = guard;
        }

        public string CmdName
        {
            get { return "SSF"; }
        }

        public void Execute(SerialClient controller, byte[] request)
        {
            Console.WriteLine(CmdName + "> " + Encoding.UTF8.GetString(request));
            this.guard.RaiseSignal(GuardManagerEventArgs.FAIL);
        }
    }
}
