﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Xml;

using log4net;

namespace DisplayGuard.Command
{
    public class UpdateLayoutCommand : ICommand
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(UpdateLayoutCommand));

        public const string NAME = "UPDLAYOUT";

        public string LayoutDir { get; set; }

        public string LayoutXml { get; set; }

        public string FileName { get; set; }

        public bool Execute()
        {
            if (string.IsNullOrEmpty(LayoutXml))
            {
                logger.Info("UpdateLayout> nodata");
                return false;
            }

            try
            {
                logger.Info(string.Format("UpdateLayout> {0:s}{1:s}.xml", LayoutDir, FileName));
                logger.Info(LayoutXml);

                string localFile = this.LayoutDir + FileName + ".xml";
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(LayoutXml);
                xdoc.Save(localFile);
                return true;
            }
            catch (Exception ex)
            {
                logger.Error("UpdateLayout > failure.", ex);
                return false;
            }
        }
    }
}
