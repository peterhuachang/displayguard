﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIA.Comm;

namespace DisplayGuard.Command
{
    public class D1SuccessCommand : IMessageCallIn<SerialClient>
    {
        private GuardManager guard;

        public D1SuccessCommand(GuardManager guard)
        {
            this.guard = guard;
        }

        public string CmdName
        {
            get { return "D1S"; }
        }

        public void Execute(SerialClient controller, byte[] request)
        {
            Console.WriteLine(CmdName + "> " + Encoding.UTF8.GetString(request));
        }
    }
}
