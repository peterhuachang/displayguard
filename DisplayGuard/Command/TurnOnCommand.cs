﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace DisplayGuard.Command
{
    public class TurnOnCommand : ICommand
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(TurnOnCommand));

        public const string NAME = "TURNON";

        public bool Execute()
        {
            logger.Info("TurnOn");
            try
            {
                //QFT.MonitorControl.IRControl.WDHIR8320Control ir = new MonitorControl.IRControl.WDHIR8320Control("COM1");
                //if (ir.Connect())
                //{
                //    ir.SendPowerControlCommand();
                //    ir.Disconnect();
                //}
                //else
                //{
                //    logger.Warn("TurnOn> Connect IR failure");
                //}
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
            return true;
        }
    }
}
