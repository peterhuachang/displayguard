﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIA.Comm;

namespace DisplayGuard.Command
{
    public class HDMIOnFailCommand : IMessageCallIn<SerialClient>
    {
        private GuardManager guard;

        public HDMIOnFailCommand(GuardManager guard)
        {
            this.guard = guard;
        }

        public string CmdName
        {
            get { return "HOF"; }
        }

        public void Execute(SerialClient controller, byte[] request)
        {
            Console.WriteLine(CmdName + "> " + Encoding.UTF8.GetString(request));
            this.guard.RaiseHdmiEvent(GuardManagerEventArgs.OFF);
        }
    }
}
