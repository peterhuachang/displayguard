﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIA.Comm;

namespace DisplayGuard.Command
{
    public class PowerStateOnCommand : IMessageCallIn<SerialClient>
    {
        private GuardManager guard;

        public PowerStateOnCommand(GuardManager guard)
        {
            this.guard = guard;
        }

        public string CmdName
        {
            get { return "PSON"; }//Power State On
        }

        public void Execute(SerialClient controller, byte[] request)
        {
            this.guard.RaisePowerEvent(GuardManagerEventArgs.ON);
        }
    }
}
