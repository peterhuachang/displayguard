﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DisplayGuard.Command
{
    public interface ICommand
    {
        bool Execute();
    }
}
