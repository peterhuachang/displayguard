﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIA.Comm;

namespace DisplayGuard.Command
{
    public class HDMIOnSuccessCommand : IMessageCallIn<SerialClient>
    {
        private GuardManager guard;

        public HDMIOnSuccessCommand(GuardManager guard)
        {
            this.guard = guard;
        }

        public string CmdName
        {
            get { return "HOS"; }
        }

        public void Execute(SerialClient controller, byte[] request)
        {
            Console.WriteLine(CmdName + "> " + Encoding.UTF8.GetString(request));
            this.guard.RaiseHdmiEvent(GuardManagerEventArgs.ON);
        }
    }
}
