﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIA.Comm;

namespace DisplayGuard.Command
{
    public class PowerOnErrorCommand : IMessageCallIn<SerialClient>
    {
        private GuardManager guard;

        public PowerOnErrorCommand(GuardManager guard)
        {
            this.guard = guard;
        }

        public string CmdName
        {
            get { return "POE"; }
        }

        public void Execute(SerialClient controller, byte[] request)
        {
            this.guard.RaiseError(GuardManagerEventArgs.ERROR);
            Console.WriteLine(CmdName + "> " + Encoding.UTF8.GetString(request)+" Power On Error");
        }
    }
}
