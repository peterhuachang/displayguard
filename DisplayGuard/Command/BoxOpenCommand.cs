﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIA.Comm;

namespace DisplayGuard.Command
{
    public class BoxOpenCommand : IMessageCallIn<SerialClient>
    {
        private GuardManager guard;

        public BoxOpenCommand(GuardManager guard)
        {
            this.guard = guard;
        }

        public string CmdName
        {
            get { return "BO"; }
        }

        public void Execute(SerialClient controller, byte[] request)
        {         
            this.guard.RaiseOpen(GuardManagerEventArgs.ALARM);
        }
    }
}
