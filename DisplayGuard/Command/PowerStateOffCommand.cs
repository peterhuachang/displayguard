﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIA.Comm;

namespace DisplayGuard.Command
{
    public class PowerStateOffCommand : IMessageCallIn<SerialClient>
    {
        private GuardManager guard;

        public PowerStateOffCommand(GuardManager guard)
        {
            this.guard = guard;
        }

        public string CmdName
        {
            get { return "PSOFF"; }//Power State OFF
        }

        public void Execute(SerialClient controller, byte[] request)
        {
            this.guard.RaisePowerEvent(GuardManagerEventArgs.OFF);
        }
    }
}
